# Proj0-Hello
-------------

## MY INSTRUCTIONS

Author: Justin Becker
Contact: jbecker4@uoregon.edu

- This program is designed to print the message specified in the user created credentials.ini (based on credentials-skel.ini). credentials.ini must be placed in the hello folder for the program to run correctly.
